# Determnizace [determination.cpp]
Úkolem této bodované programovací úlohy je naimplementovat algoritmy, které vám umožní nalezení minimálního deterministického konečného automatu, který přijímá stejný jazyk jako zadaný libovolný nedeterministický konečný automat s více počátečními stavy. Cílem je tedy implementovat sadu tří funkcí v jazyce C++ pro determinizaci, odstranění zbytečných a nedosažitelných stavů a minimalizaci. Signatury těchto tří funkcí jsou:

- DFA determinize ( const MISNFA & );,
- DFA trim ( const DFA & ); a
- DFA minimize ( const DFA & );.

V první části, tedy v této úloze, je úkolem naimplementovat pouze determinizaci libovolného nedeterministického automatu s více počátečními stavy. 



# Odstranění nedosažitelných a zbytečných stavů [trim.cpp]
Cílem této úlohy je implementace algoritmů determinizace (kterou byste měli mít hotovou již z předchozí úlohy) a odstranění zbytečných a nedosažitelných stavů. Tedy implementace následujících funkcí:

- DFA determinize ( const MISNFA & );
- DFA trim ( const DFA & );

