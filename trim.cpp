#ifndef __PROGTEST__
#include <algorithm>
#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <deque>
#include <iostream>
#include <list>
#include <map>
#include <memory>
#include <numeric>
#include <optional>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <vector>
using namespace std;

using State = unsigned int;
using Symbol = char;

struct MISNFA {
	std::set < State > m_States;
	std::set < Symbol > m_Alphabet;
	std::map < std::pair < State, Symbol >, std::set < State > > m_Transitions;
	std::set < State > m_InitialStates;
	std::set < State > m_FinalStates;
};

struct DFA {
	std::set < State > m_States;
	std::set < Symbol > m_Alphabet;
	std::map < std::pair < State, Symbol >, State > m_Transitions;
	State m_InitialState;
	std::set < State > m_FinalStates;

	bool operator== ( const DFA & other ) {
	return
		std::tie ( m_States, m_Alphabet, m_Transitions, m_InitialState, m_FinalStates ) ==
		std::tie ( other.m_States, other.m_Alphabet, other.m_Transitions, other.m_InitialState, other.m_FinalStates );
	}
};

void output(const DFA & dfa )
{
    cout << "States : ";
    for (const State & state : dfa.m_States)
    { cout << state << " "; }
    cout << endl;

    cout << "Transitions------------------ "<<endl;
    cout << "Alphabet : ";
    for(const Symbol & symbol : dfa.m_Alphabet)
    { cout << symbol << " "; }
    cout << endl;
    for (const State &state : dfa.m_States)
    {
        cout << "State " << state << ": ";
        for(const Symbol & symbol : dfa.m_Alphabet)
        {
            if ( dfa.m_Transitions.find(make_pair(state, symbol)) != dfa.m_Transitions.end())
            {
                cout <<dfa.m_Transitions.find(make_pair(state, symbol))->second << " ";
            }
        }
        cout << endl;
    }
    cout << endl;

    cout << "Initial State : " << dfa.m_InitialState << endl;

    cout << "Final States : ";
    for (const State & state : dfa.m_FinalStates)
    { cout << state << " "; }
    cout << endl;
}

#endif



// TODO: Implement these functions

DFA determinize ( const MISNFA & nfa )
{
    MISNFA copy_nfa = nfa;
    DFA dfa;
    dfa.m_InitialState = 0;
    dfa.m_Alphabet.insert(copy_nfa.m_Alphabet.begin(),copy_nfa.m_Alphabet.end());

    //New names
    std::map <std::set <State>, State> newStates;

    //New initial state is always 0, represents all old init states
    newStates.insert(std::make_pair(copy_nfa.m_InitialStates,dfa.m_InitialState));

    std::set<State> comb;

    std::queue<std::set<State>> needDiscover;
    needDiscover.push(copy_nfa.m_InitialStates);
    while (!needDiscover.empty())
    {
        for (const State & final : copy_nfa.m_FinalStates)
        {
            if(needDiscover.front().find(final) != needDiscover.front().end())
            { dfa.m_FinalStates.insert(newStates.find(needDiscover.front())->second); }
        }
        /*
        cout << "Current State to discover: ";
        for (const State & state : needDiscover.front()) {
              cout << state << " ";
        }
        cout << endl;
         */
        for(const Symbol & symbol : copy_nfa.m_Alphabet)
        {
            for (const State & state : needDiscover.front())
            {
                if (copy_nfa.m_Transitions.find(std::make_pair(state, symbol))!=copy_nfa.m_Transitions.end())
                {
                    std::set<State> transition = copy_nfa.m_Transitions.find(std::make_pair(state, symbol)) -> second;
                    comb.insert(transition.begin(),transition.end());
                }
            }

            if (newStates.find(comb)!=newStates.end())
            { dfa.m_Transitions.insert(std::make_pair(std::make_pair((newStates.find(needDiscover.front())->second),symbol), newStates.find(comb)->second)); }
            else
            {
                needDiscover.push(comb);
                dfa.m_Transitions.insert(std::make_pair(std::make_pair((newStates.find(needDiscover.front())->second),symbol), newStates.size()));
                newStates.insert(std::make_pair(comb, newStates.size()));
            }

            comb.clear();
        }
        needDiscover.pop();
    }
    for (int i = 0; i < (int)newStates.size(); ++i)
    { dfa.m_States.insert(i); }

    //output(dfa);
    return dfa;
}

void deleteStates(DFA & dfa, const set<State>& toDelete)
{
    for (const State & state : toDelete)
    { dfa.m_States.erase(state); }

    auto it = dfa.m_Transitions.begin();
    while (it != dfa.m_Transitions.end())
    {
        if( toDelete.find(it -> second) != toDelete.end() ||
            toDelete.find(it -> first.first) != toDelete.end() )
        { it = dfa.m_Transitions.erase(it); }
        else
        { ++it; }
    }
}
void trimUnnecessary(DFA & dfa)
{
    set<State> notVisited = dfa.m_States;
    queue<State> needDiscover;
    for (const State & state : dfa.m_FinalStates)
    { needDiscover.push(state); }

    while(!needDiscover.empty())
    {
        State cur = needDiscover.front();
        needDiscover.pop();
        if (notVisited.find(cur) == notVisited.end())
        { continue; }

        notVisited.erase(cur);

        auto it = dfa.m_Transitions.begin();
        while (it != dfa.m_Transitions.end())
        {
            if( it->second == cur )
            { needDiscover.push(it->first.first); }
            ++it;
        }
    }

    deleteStates(dfa, notVisited);
}

void trimUnAccessable(DFA & dfa)
{
    set<State> notVisited = dfa.m_States;
    queue<State> needDiscover;
    needDiscover.push(dfa.m_InitialState);
    while (!needDiscover.empty())
    {
        State cur = needDiscover.front();
        needDiscover.pop();
        if (notVisited.find(cur) == notVisited.end())
        { continue; }

        notVisited.erase(cur);

        for(const Symbol & symbol : dfa.m_Alphabet)
        {
            auto it = dfa.m_Transitions.find(std::make_pair(cur, symbol));
            if (it != dfa.m_Transitions.end())
            { needDiscover.push(it->second); }
        }
    }

    for (const State & state : notVisited)
    { dfa.m_FinalStates.erase(state); }

    deleteStates(dfa, notVisited);
}


DFA trim ( const DFA & dfa )
{
    DFA newDFA = dfa;
    if (dfa.m_Alphabet.empty() || dfa.m_FinalStates.empty())
    {
        return DFA{
                { 0 },
                { dfa.m_Alphabet},
                {},
                0,
                { },
        };
    }
    else
    {
        trimUnAccessable(newDFA);
        trimUnnecessary(newDFA);
    }

    //output(newDFA);
    return newDFA;
}

DFA minimize ( const DFA & dfa ) { return dfa; }


#ifndef __PROGTEST__

#include "sample.h"

int main ( ) {
	/* IMPORTANT NOTE:
	 *
	 * Do not forget that automata equivalence (i.e., the regular language equivalence) is algorithmically decidable by
	 * checking for the isomorphism of two minimal DFAs.
	 *
	 * Your determinization algorithm *may* give you a result that is different from the test output used in the asserts below.
	 * This *may* not be wrong. If the automaton still accepts the same language, it will be accepted by Progtest. Progtest
	 * will minimize the automaton you returned from determinize() and/or trim() functions and compare it with the reference solution.
	 *
	 * Also note that the naming of the states does not play a role.
	 * The solutions (outD/outT/outM) for the simple "assert" tests are based upon one of our reference solutions.
	 * It is very much possible that your solutions uses a different naming scheme.
	 * Progtest accepts automata that use a different naming scheme.
	 *
	 * If your are unsure about the correct result of any algorithm on any input, you are welcome to use https://alt.fit.cvut.cz/webui/ tool.
	 */

	// determinize
   // assert (
       //     determinize ( in15 );
            //== outD14 );
    assert ( determinize ( in0 ) == outD0 );
    assert ( determinize ( in1 ) == outD1 );
    assert ( determinize ( in2 ) == outD2 );
    assert ( determinize ( in3 ) == outD3 );
    assert ( determinize ( in4 ) == outD4 );
    assert ( determinize ( in5 ) == outD5 );
    assert ( determinize ( in6 ) == outD6 );
    assert ( determinize ( in7 ) == outD7 );
    assert ( determinize ( in8 ) == outD8 );
    assert ( determinize ( in9 ) == outD9 );
    assert ( determinize ( in10 ) == outD10 );
    assert ( determinize ( in11 ) == outD11 );
    assert ( determinize ( in12 ) == outD12 );
    assert ( determinize ( in13 ) == outD13 );

    // trim
 //   assert ( trim ( determinize ( in0 ) ) == outT0 );
  //  assert ( trim ( determinize ( in1 ) ) == outT1 );
    assert ( trim ( determinize ( in2 ) ) == outT2 );
   /* assert ( trim ( determinize ( in3 ) ) == outT3 );
    assert ( trim ( determinize ( in4 ) ) == outT4 );
    assert ( trim ( determinize ( in5 ) ) == outT5 );
    assert ( trim ( determinize ( in6 ) ) == outT6 );
    assert ( trim ( determinize ( in7 ) ) == outT7 );
    assert ( trim ( determinize ( in8 ) ) == outT8 );
    assert ( trim ( determinize ( in9 ) ) == outT9 );
    assert ( trim ( determinize ( in10 ) ) == outT10 );
    assert ( trim ( determinize ( in11 ) ) == outT11 );
    assert ( trim ( determinize ( in12 ) ) == outT12 );
    assert ( trim ( determinize ( in13 ) ) == outT13 );


    // minimize
    assert ( minimize ( trim ( determinize ( in0 ) ) ) == outM0 );
    assert ( minimize ( trim ( determinize ( in1 ) ) ) == outM1 );
    assert ( minimize ( trim ( determinize ( in2 ) ) ) == outM2 );
    assert ( minimize ( trim ( determinize ( in3 ) ) ) == outM3 );
    assert ( minimize ( trim ( determinize ( in4 ) ) ) == outM4 );
    assert ( minimize ( trim ( determinize ( in5 ) ) ) == outM5 );
    assert ( minimize ( trim ( determinize ( in6 ) ) ) == outM6 );
    assert ( minimize ( trim ( determinize ( in7 ) ) ) == outM7 );
    assert ( minimize ( trim ( determinize ( in8 ) ) ) == outM8 );
    assert ( minimize ( trim ( determinize ( in9 ) ) ) == outM9 );
    assert ( minimize ( trim ( determinize ( in10 ) ) ) == outM10 );
    assert ( minimize ( trim ( determinize ( in11 ) ) ) == outM11 );
    assert ( minimize ( trim ( determinize ( in12 ) ) ) == outM12 );
    assert ( minimize ( trim ( determinize ( in13 ) ) ) == outM13 );
*/
	return 0;
}
#endif
